package clase;

public abstract class Blueprint
{
   public double material;
   public double volum;

   public Blueprint(double paramMaterial, double paramVolum)
   {
       this.material = paramMaterial;
       this.volum = paramVolum;
   }

  public abstract double  densitate();
}