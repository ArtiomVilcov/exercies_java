package clase;



public class Ferrari extends Masina implements Function
{


    public String model;

    public Ferrari(String paramCuloare, int paramLungime, int paramPutere, String paramModel)
    {
        super(paramCuloare,paramLungime, paramPutere);
        this.model= paramModel;
    }



    public void Start()
    {
        System.out.println("Car starts engine");

    }

    public void Stop()
    {
        System.out.println("Car stops engine");
    }
}
