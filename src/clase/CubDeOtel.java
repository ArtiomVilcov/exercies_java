package clase;



public  class CubDeOtel extends Blueprint
{
    public CubDeOtel(double paramMasaMaterial, double paramVolum)
    {
        super(paramMasaMaterial,paramVolum);
    }

    public double densitate()
    {
         double Dens = this.material / this.volum;
        return Dens;
    }
}